<?php
/**
 * Created by PhpStorm.
 * User: flank
 * Date: 08.02.18
 * Time: 14:59
 */

namespace App\Providers;

use App\Category;
use Collective\Html\FormFacade as Form;

use Illuminate\Support\ServiceProvider;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::macro('categories', function ($data = []) {

            $selected = $data['selected'] ??  old('category_id');
            $categories = Category::all()->sortBy('name');
            $categories_json = [];
            $categories_json[0] = 'Не выбран';

            foreach($categories as $category) {
                $categories_json[$category->id] = $category->name;
            }
            return Form::select('category_id', $categories_json, $selected, ['class' => 'full-width', 'id' => 'category_id']);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}