<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'name',
        'body',
        'category_id',
        'user_id'
    ];

    public function user() {
        return $this->hasOne(User::class);
    }

    public function category() {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
