@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($posts as $post)
            <div class="card mb-3">
                <div class="card-header">
                    <h3><a href="/posts/{{ $post->id }}">{{ $post->name }}</a></h3>
                </div>

                <div class="card-body">
                    {{ $post->body }}
                </div>

                <div class="card-footer">
                    {{ $post->created_at->format('d.m.Y') }}
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
