@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">



        <div class="col-sm-3">

            <a href="/posts/create" class="btn btn-success">Добавить</a>

            <h3>Фильтр</h3>
            <form action="/posts" method="get">
                <label>Категория</label>
                {{ Form::categories(['selected' => '']) }}
                <input type="submit" class="form-control btn btn-success mb-3" value="Применить" />
                <a href="/posts" class="form-control btn btn-info">Сбросить</a>
            </form>
        </div>
        <div class="col-md-9">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Название</td>
                        <td>Категория</td>
                        {{--<td>Текст</td>--}}
                        <td>Действия</td>
                    </tr>
                </thead>
                @foreach($posts as $post)
                <tbody>
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->name }}</td>
                        <td>{{ $post->category->name }}</td>
                        {{--<td>{{ $post->body }}</td>--}}
                        <td><a href="/posts/{{ $post->id }}/edit"><i class="fa fa-edit"></i> Редактировать </a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $posts->links() }}
            Всего записей найдено: {{ $posts->total() }}
        </div>
    </div>
</div>
@endsection
