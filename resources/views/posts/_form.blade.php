<div class="form-group clearfix @if ($errors->has('name')) has-error @endif">
    <div class="col-sm-12">
        <div class="col-sm-4">
            {{ Form::label('name', 'Название') }}
        </div>
        <div class="col-sm-4">
            {{ Form::text('name', $item->name  ?? '', ['id' => 'name', 'class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="form-group clearfix @if ($errors->has('body')) has-error @endif">
    <div class="col-sm-12">
        <div class="col-sm-4">
            {{ Form::label('description', 'Текст') }}
        </div>
        <div class="col-sm-4">
            {{ Form::textarea('body', $item->body  ?? '', ['id' => 'body', 'class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="form-group clearfix @if ($errors->has('category_id')) has-error @endif">
    <div class="col-sm-12">
        <div class="col-sm-4">
            {{ Form::label('category_id', 'Категория',['class' => 'form-label']) }}
        </div>
        <div class="col-sm-4">
            {{ Form::categories(['selected' => '']) }}
        </div>
    </div>
</div>
