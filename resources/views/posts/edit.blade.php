@extends('layouts.app')

@section('content')
    <div class="container">
    {!! Form::model('post', ['method' => 'PUT', 'route' => ['posts.update', $item->id ] ]) !!}
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $item->id }}" />
        @include('posts._form')
        <div class="form-group clearfix">
            <div class="col-sm-12">
                <div class="col-sm-4">
                    &nbsp;
                </div>
                <div class="col-sm-4">
                    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection