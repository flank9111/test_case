@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-md-5">
            <h3>Добавление</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('posts.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Назад</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        {!! Form::open(['route' => ['posts.store'] ]) !!}
            @include('posts._form')
            <div class="form-group clearfix">
                <div class="col-sm-12">
                    <div class="col-sm-4">
                        &nbsp;
                    </div>
                    <div class="col-sm-4">
                        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection